# Gitlab CI Templates

This repository contains set of job templates for easier deployments and tests. You can use the following job templates:

## Job templates included:

### jobs/composer.yml

This job is based on `composer:2` image and contains three templates:

* `.composer` that contains composer and installs `wpify/scoper` globally. It also authenticates composer to satisspress if the variables `$SATISPRESS_URL`, `$SATISSPRESS_USERNAME` and `$SATISSPRESS_PASSWORD` are defined.
* `.composer_installs` runs command `composer install --prefer-dist --optimize-autoloader --ignore-platform-reqs --no-ansi --no-interaction --no-dev`.
* `.composer_update` runs command `composer update --prefer-dist --optimize-autoloader --ignore-platform-reqs --no-ansi --no-interaction --no-dev`.

**Usage `.gitlab-ci.yml`:**

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/composer.yml'

composer:
  stage: .pre
  extends: .composer_update
  artifacts:
    paths:
      - $CI_PROJECT_DIR/deps
      - $CI_PROJECT_DIR/vendor
      - $CI_PROJECT_DIR/composer.json
      - $CI_PROJECT_DIR/composer.lock
  expire_in: 1 week
```

### jobs/nvm.yml

This job installs Node Version Manager to manage node version. It uses `.nvmrc` file to idetify the correct node version for the project. If there is no `.nvmrc` file present, it uses the latest LTS version of node. If there is a `package.json` file in the project root, it runs `npm ci` command for you. After that, the template runs command `NODE_ENV=production npm run build --if-present`, but you can overwrite that by defining own script in job definition.

**Usage `.gitlab-ci.yml`:**

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/nvm.yml'

assets:
  stage: .pre
  extends: .nvm
  artifacts:
    paths:
      - $CI_PROJECT_DIR/build
      - $CI_PROJECT_DIR/themes/project-theme/style.css
      - $CI_PROJECT_DIR/themes/project-theme/editor-style.css
    expire_in: 1 week
```

if you want to overwrite default script, use e.g. the following:

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/nvm.yml'

assets:
  stage: .pre
  extends: .nvm
  artifacts:
    paths:
      - $CI_PROJECT_DIR/build
      - $CI_PROJECT_DIR/themes/project-theme/style.css
      - $CI_PROJECT_DIR/themes/project-theme/editor-style.css
    expire_in: 1 week
  script: npm run build:production
```

### jobs/cypress.yml

This job template is meant for end-to-end testing in the pipeline. For example after upgrading dependencies, plugins, core, deployment, etc. It expects cypress tests in `cypress` folder and `cypress.config.js` file in project root. The following artifacts are stored:

* cypress/videos/**/*.mp4
* cypress/screenshots/**/*.png
* cypress/downloads/**/*.*

The commands that are run are:

* npm ci
* npx cypress run --browser chrome

**Usage `.gitlab-ci.yml`:**

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/cypress.yml'

test:
  stage: .post
  extends: .cypress
  variables:
    CYPRESS_URL: $ENV_URL
    CYPRESS_USERNAME: $ENV_USERNAME
    CYPRESS_PASSWORD: $ENV_PASSWORD
```

If you use any variables that are prefixed by `CYPRESS_`, you can use its values in your tests:

```javascript
/// <reference types="cypress" />

describe('Some project', () => {
  beforeEach(() => {
    cy.viewport(1200, 900);
    const username = Cypress.env('USERNAME');
    const password = Cypress.env('PASSWORD');
    const url = Cypress.env('URL') || 'https://www.some-project.test/';
    const options = { failOnStatusCode: false };

    if (username && password) {
      options.auth = { username, password }
    }

    cy.visit(url, options);
  });
});
```

### jobs/git.yml

This job template helps you with committing the changes to the repository itself and tagging patch version automatically. Template requires variables `$GIT_EMAIL`, `$GIT_NAME` and `$PRIVATE_KEY` variables set.

**Usage `.gitlab-ci.yml`:**

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/git.yml'

commit:
  stage: .post
  extends: .git_commit_tag
  variables:
    GIT_NAME: Gitlab CI
    GIT_EMAIL: gitlab-ci@wpify.io
    PRIVATE_KEY: some private key
```

### jobs/tools.yml

This is the most versatile templates of all. It provides plenty of functions that make deployments easy.

**Usage `.gitlab-ci.yml`**

All the examples bellow comes into `scripts` section of the job:

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/tools.yml'

deploy:
  stage: deploy
  extends: .tools
  script: [here are the scripts]
```

**plugin_archive**

Function that makes plugin zip archive file. The function requires `$PLUGIN_SLUG` variable set.

```shell
plugin_archive wp-cli.yml config/ vendor/ web/*.php ...
```

**replace**

Function replaces one string with another within the file. Make sure that you don't use special characters within the search parameter. The following exampla replaces `WPIFY_VERSION` with the current tag or branch name in `plugin-file.php`.

```shell
replace "WPIFY_VERSION" "$CI_COMMIT_REF_NAME" "plugin-file-1.php" "plugin-file-2.php" ...
```

**server_run**

Function that runs command on the remote server via ssh. Function requires variables `$SERVER_ADDR`, `$PRIVATE_KEY`, `$SERVER_USER` and `$SERVER_PATH`.

```shell
server_run "wp cache flush && wp rewrite flush"
```

**server_deploy**

Deploys files via rsync on the remote server. Function requires variables `$SERVER_ADDR`, `$PRIVATE_KEY`, `$SERVER_USER` and `$SERVER_PATH`. It rsync files from project root to the server's `$SERVER_PATH`. Rsync runs with `-a --delete --no-perms --no-owner --no-group` flags. If the remote folder doesn't exists, it will be created in the process.

```shell
server_deploy wp-cli.yml config/  vendor/ web/*.php web/app/mu-plugins/ ...
```

**wporg_plugin_deploy**

Deploys a plugin to WordPress.org. The function requires `$WPORG_USERNAME`, `$WPORG_PASSWORD`, `$PLUGIN_SLUG` and `$WPORG_ASSETS`. The last variable `$WPORG_ASSETS` represents a folder in project that contains assets for the plugin, such as plugin icon, cover image, etc. The folder must not have a trailing slash.

```shell
wporg_plugin_deploy
```

**mailgun**

Sends email via mailgun.com service. The function requires `$MAILGUN_API_KEY`, `$MAILGUN_DOMAIN` and `$MAILGUN_FROM`. The function requires parameters to, subject and body.

```shell
mailgun "test@wpify.io" "Example subject of the email" "Example body of the email, this is awesome!"
```

## Example pipelines

### Deploying roots.io project

```yaml
include:
  - project: 'wpify/gitlab-ci-templates'
    ref: master
    file:
      - 'jobs/nvm.yml'
      - 'jobs/composer.yml'
      - 'jobs/tools.yml'
      - 'jobs/cypress.yml'

variables:
  PROJECT_NAME: some-project-name

assets:
  stage: .pre
  extends: .nvm
  artifacts:
    paths:
      - $CI_PROJECT_DIR/web/app/mu-plugins/$PROJECT_NAME/build
      - $CI_PROJECT_DIR/web/app/themes/$PROJECT_NAME/style.css
      - $CI_PROJECT_DIR/web/app/themes/$PROJECT_NAME/editor-style.css
    expire_in: 1 week

composer:
  stage: .pre
  extends: .composer_install
  artifacts:
    paths:
      - $CI_PROJECT_DIR/deps
      - $CI_PROJECT_DIR/vendor
      - $CI_PROJECT_DIR/web/app/mu-plugins
      - $CI_PROJECT_DIR/web/app/plugins
      - $CI_PROJECT_DIR/web/app/themes
      - $CI_PROJECT_DIR/web/app/vendor
      - $CI_PROJECT_DIR/web/wp
    expire_in: 1 week

deploy:
  stage: deploy
  extends: .tools
  artifacts:
    paths:
      - $CI_PROJECT_DIR/$PROJECT_NAME.zip
    expire_in: 1 week
  environment:
    name: $ENV_NAME
    url: $ENV_URL
  needs:
    - assets
    - composer
  script: |
    replace "WPIFY_VERSION" "$CI_COMMIT_REF_NAME" "web/app/mu-plugins/$PROJECT_NAME/$PROJECT_NAME.php"
    replace "WPIFY_VERSION" "$CI_COMMIT_REF_NAME" "web/app/themes/$PROJECT_NAME/style.css"

    server_deploy \
      wp-cli.yml \
      config/ \
      vendor/ \
      web/*.php \
      web/app/mu-plugins/ \
      web/app/plugins/ \
      web/app/themes/ \
      web/app/vendor/ \
      web/app/vendor/ \
      web/wp/*.php \
      web/wp/wp-admin/ \
      web/wp/wp-includes/

    server_run "wp cache flush && wp rewrite flush"
    server_run "rm -rf $SERVER_PATH/web/app/cache"
    server_run "/usr/bin/sudo /usr/local/sbin/purge_cache.sh"
  
test:
  stage: .post
  extends: .cypress
  variables:
    CYPRESS_URL: $ENV_URL
    CYPRESS_USERNAME: $ENV_USERNAME
    CYPRESS_PASSWORD: $ENV_PASSWORD
  needs:
    - deploy
```

